package com.experis;

import com.experis.attributes.CharecterName;
import com.experis.attributes.PrimaryAttributes;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //Prints Default values of Rogue at level 1
        Hero character = new Hero();
        character.setPlayerClass(CharecterName.ROGUE);
        character.classAttributes(CharecterName.ROGUE, 1);
        System.out.println("ROGUE's Basic Attributes at Level 1:");
        System.out.println("Vitality:"+character.getPrimary().getVitality()+"  Strength:"+
                character.getPrimary().getStrength()+ "  Dexterity:"+character.getPrimary().getDexterity()+
                "  Intelligence:"+ character.getPrimary().getIntelligence());
        //Prints Warrior's Basic attributes
        character.setPlayerClass(CharecterName.WARRIOR);
        character.classAttributes(CharecterName.WARRIOR, 1);
        System.out.println("WARRIOR's Basic Attributes at Level 1:");
        System.out.println("Vitality:"+character.getPrimary().getVitality()+"  Strength:"+
                character.getPrimary().getStrength()+ "  Dexterity:"+character.getPrimary().getDexterity()+
                "  Intelligence:"+ character.getPrimary().getIntelligence());
        //Prints MAGE's Basic attributes
        character.setPlayerClass(CharecterName.MAGE);
        character.classAttributes(CharecterName.MAGE, 1);
        System.out.println("MAGE's Basic Attributes at Level 1:");
        System.out.println("Vitality:"+character.getPrimary().getVitality()+"  Strength:"+
                character.getPrimary().getStrength()+ "  Dexterity:"+character.getPrimary().getDexterity()+
                "  Intelligence:"+ character.getPrimary().getIntelligence());
        //Prints RANGER's Basic attributes
        character.setPlayerClass(CharecterName.RANGER);
        character.classAttributes(CharecterName.RANGER, 1);
        System.out.println("RANGER's Basic Attributes at Level 1:");
        System.out.println("Vitality:"+character.getPrimary().getVitality()+"  Strength:"+
                character.getPrimary().getStrength()+ "  Dexterity:"+character.getPrimary().getDexterity()+
                "  Intelligence:"+ character.getPrimary().getIntelligence());

        //Prints Warrior's Heallth,ArmorRarting and resistance at level 2
        character.setPlayerClass(CharecterName.WARRIOR);
        character.classAttributes(CharecterName.WARRIOR, 1);
        character.setCharacterDamageBonus(CharecterName.WARRIOR);
        character.levelUp(character.getPlayerClass(), 1);
        System.out.println("------------------------------------------------------");
        System.out.println("Warrior Status at Level 2:");
        System.out.println("Health:"+character.getHealth()+"  ArmorRating:"+
                character.getArmorRating()+ "  Resistance:"+
                character.getElementalResistance());


        //Prints Rogue's Health,ArmorRarting and resistance at level 2
        character.setPlayerClass(CharecterName.ROGUE);
        character.classAttributes(CharecterName.ROGUE, 1);
        character.setCharacterDamageBonus(CharecterName.ROGUE);
        character.levelUp(character.getPlayerClass(), 1);
        System.out.println("------------------------------------------------------");
        System.out.println("Rogue Status at Level 2:");
        System.out.println("Health:"+character.getHealth()+"  ArmorRating:"+
                character.getArmorRating()+ "  Resistance:"+
                character.getElementalResistance());
        //Prints Ranger's Health,ArmorRarting and resistance at level 2
        character.setPlayerClass(CharecterName.RANGER);
        character.classAttributes(CharecterName.RANGER, 1);
        character.setCharacterDamageBonus(CharecterName.RANGER);
        character.levelUp(character.getPlayerClass(), 1);
        System.out.println("------------------------------------------------------");
        System.out.println("Ranger Status at Level 2:");
        System.out.println("Health:"+character.getHealth()+"  ArmorRating:"+
                character.getArmorRating()+ "  Resistance:"+
                character.getElementalResistance());

        //Prints Mage's Health,ArmorRarting and resistance at level 2
        character.setPlayerClass(CharecterName.MAGE);
        character.classAttributes(CharecterName.MAGE, 1);
        character.setCharacterDamageBonus(CharecterName.MAGE);
        character.levelUp(character.getPlayerClass(), 1);
        System.out.println("------------------------------------------------------");
        System.out.println("Mage Status at Level 2:");
        System.out.println("Health:"+character.getHealth()+"  ArmorRating:"+
                character.getArmorRating()+ "  Resistance:"+
                character.getElementalResistance());
    }
}

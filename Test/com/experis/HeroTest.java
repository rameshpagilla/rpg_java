package com.experis;

import com.experis.attributes.CharecterName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    void mageDefaultAttributes_levelOne_true(){
        //Creating an instance of a character "attributeChecker" of playerClass MAGE, starting at level 1. Used to test character systems functionality.
        var attributeChecker = new Hero();
        attributeChecker.setPlayerClass(CharecterName.MAGE);
        attributeChecker.classAttributes(CharecterName.MAGE, 1);
        attributeChecker.setCharacterDamageBonus(CharecterName.MAGE);
        int expected[] = new int[]{5,1,1,8};
        int classAttributesAtLevelOne[] = new int[]{attributeChecker.getPrimary().getVitality(), attributeChecker.getPrimary().getStrength()
                ,attributeChecker.getPrimary().getDexterity(), attributeChecker.getPrimary().getIntelligence()};
        assertArrayEquals(expected, classAttributesAtLevelOne);
    }
    @Test
    void warriorDefaultAttributes_levelOne_true(){
        //Creating an instance of a character "attributeChecker" of playerClass WARRIOR, starting at level 1. Used to test character systems functionality.
        var attributeChecker = new Hero();
        attributeChecker.setPlayerClass(CharecterName.WARRIOR);
        attributeChecker.classAttributes(CharecterName.WARRIOR, 1);
        attributeChecker.setCharacterDamageBonus(CharecterName.WARRIOR);
        int expected[] = new int[]{10,5,2,1};
        int classAttributesAtLevelOne[] = new int[]{attributeChecker.getPrimary().getVitality(), attributeChecker.getPrimary().getStrength()
                ,attributeChecker.getPrimary().getDexterity(), attributeChecker.getPrimary().getIntelligence()};
        assertArrayEquals(expected, classAttributesAtLevelOne);
    }
    @Test
    void rangerDefaultAttributes_levelOne_true(){
        //Creating an instance of a character "attributeChecker" of playerClass RANGER, starting at level 1. Used to test character systems functionality.
        var attributeChecker = new Hero();
        attributeChecker.setPlayerClass(CharecterName.RANGER);
        attributeChecker.classAttributes(CharecterName.RANGER, 1);
        attributeChecker.setCharacterDamageBonus(CharecterName.RANGER);
        int expected[] = new int[]{8,1,7,1};
        int classAttributesAtLevelOne[] = new int[]{attributeChecker.getPrimary().getVitality(), attributeChecker.getPrimary().getStrength()
                ,attributeChecker.getPrimary().getDexterity(), attributeChecker.getPrimary().getIntelligence()};
        assertArrayEquals(expected, classAttributesAtLevelOne);
    }
    @Test
    void rougeDefaultAttributes_levelOne_true(){
        //Creating an instance of a character "attributeChecker" of playerClass ROGUE, starting at level 1. Used to test character systems functionality.
        var attributeChecker = new Hero();
        attributeChecker.setPlayerClass(CharecterName.ROGUE);
        attributeChecker.classAttributes(CharecterName.ROGUE, 1);
        attributeChecker.setCharacterDamageBonus(CharecterName.ROGUE);
        int expected[] = new int[]{8,2,6,1};
        int classAttributesAtLevelOne[] = new int[]{attributeChecker.getPrimary().getVitality(), attributeChecker.getPrimary().getStrength()
                ,attributeChecker.getPrimary().getDexterity(), attributeChecker.getPrimary().getIntelligence()};
        assertArrayEquals(expected, classAttributesAtLevelOne);
    }

    @Test
    void attributesToString() {
    }

    @Test
    void secondaryAttributes_levelOneToLevelTwo_true(){

        var attributeChecker = new Hero();
        attributeChecker.setPlayerClass(CharecterName.WARRIOR);
        attributeChecker.classAttributes(CharecterName.WARRIOR, 1);
        attributeChecker.setCharacterDamageBonus(CharecterName.WARRIOR);
        attributeChecker.levelUp(attributeChecker.getPlayerClass(),1);
        int expected[] = new int[]{150,12,2};
        int classAttributesAtLevelOne[] = new int[]{attributeChecker.getHealth(), attributeChecker.getArmorRating()
                ,attributeChecker.getElementalResistance()};
        assertArrayEquals(expected, classAttributesAtLevelOne);

    }

    @Test
    void levelUpTo2_levelOneToLevelTwo_true(){
        //Creating an instance of a character "levelUpChecker" of playerClass WARRIOR, starting at level 1. Used to test character systems functionality.
        var levelUpChecker = new Hero();
        levelUpChecker.setPlayerClass(CharecterName.WARRIOR);
        levelUpChecker.classAttributes(CharecterName.WARRIOR, 1);
        levelUpChecker.setCharacterDamageBonus(CharecterName.WARRIOR);
        levelUpChecker.levelUp(levelUpChecker.getPlayerClass(),1);
        assertTrue(levelUpChecker.getLevel() == 2);
    }
    @Test
    void isLevelOne_levelOne_true(){
        var levelOneChecker = new Hero();
        assertTrue(levelOneChecker.getLevel() == 1);
    }

}
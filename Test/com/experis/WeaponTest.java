package com.experis;

import com.experis.Items.*;
import com.experis.attributes.CharecterName;
import com.experis.attributes.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    @Test
    void tooHighLevelWeaponEquip_level1PlayerLevel2Weapon_true(){
        //Creating a new instance of a weapon of weaponType AXE, name "Test Axe", slot Slot.Weapon, damage 20, attackSpeed 0.5, and level required 2.
        Weapon testAxe = new Weapon();
        testAxe.setName("Test Axe");
        testAxe.setLevelRequired(2);
        testAxe.setSlot(Slot.WEAPON);
        testAxe.setWeaponType(WeaponType.AXE);
        testAxe.setDamage(20);
        testAxe.setAttackSpeed(0.5);
        //Creating an instance of a character "itemChecker" of playerClass WARRIOR, starting at level 1. Used to test item functionality.
        var itemChecker = new Hero();
        itemChecker.setPlayerClass(CharecterName.WARRIOR);
        itemChecker.classAttributes(CharecterName.WARRIOR, 1);
        itemChecker.setCharacterDamageBonus(CharecterName.WARRIOR);
        assertThrows(InvaliedWeaponException.class, ()-> itemChecker.equipWeapon(Slot.WEAPON, testAxe));

    }

    @Test void tooHighLevelArmorEquip_level1PlayerLevel2Armor_true(){
        //Creating a new instance of armor of armorType Plate, name "Common Plate Body Armor", level required 2, slot Slot.BODY, and with primary attributes (str 1, dex 0, intl, 0, vit 2).
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setLevelRequired(2);
        testPlateBody.setSlot(Slot.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setPrimary(new PrimaryAttributes(1,0,0,2));

        var itemChecker = new Hero();
        itemChecker.setPlayerClass(CharecterName.WARRIOR);
        itemChecker.classAttributes(CharecterName.WARRIOR, 1);
        itemChecker.setCharacterDamageBonus(CharecterName.WARRIOR);

        assertThrows(InvaliedArmorException.class, ()-> itemChecker.equipArmor(Slot.BODY, testPlateBody));

    }


}